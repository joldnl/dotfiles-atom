#!/bin/bash

# Check if atom is installed with Homebrew
# if brew cask ls --versions atom > /dev/null; then
#     echo "Atom is happily installed :D"
# else
#     # The package is not installed
#     echo "Atom is not installed. Please make sure Atom is installed with Homebrew!."
#     # exit
# fi




# Install and activate atom packages from packages.txt
echo ""
echo "-------------------------------------------"
echo "| Install packages from package.txt       |"
echo "-------------------------------------------"

cd config
input="packages.txt"
while IFS= read -r var
do
    if [[ ! -d "$HOME/.atom/packages/$var" ]]
    then
        apm install $var
    else
        echo "package '$var' is already installed, skipping ..."
    fi
done < "$input"
cd ../
echo "Done! ..."



# Copy config files to .atom/ directory
echo ""
echo "-------------------------------------------"
echo "| Copy atom config files to ~/.atom/      |"
echo "-------------------------------------------"

echo "Copying config.cson, snippets.cson and styles.less to .atom folder ..."
cd config
rm -Rf packages.txt
cp -f * ~/.atom/
cd ../
echo "Done! ..."



# Install atm:// URL handler for Atom editor on OSX
echo ""
echo "-------------------------------------------"
echo "| Install Atom Handler"
echo "-------------------------------------------"

echo "Download zipfile ..."
wget https://github.com/WizardOfOgz/atom-handler/releases/download/v1.1.3/atom-handler.app.zip

unzip atom-handler.app.zip
mv atom-handler.app /Applications
rm -f atom-handler.app.zip
echo "Please run the /Applications/atom-handler.app manually from finder ..."
open /Applications/

echo "Done! ..."
